import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import{RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms'
import { AppComponent } from './app.component';


//Servicios

import {ApiService} from './services/api.service';


//Rutas
import {APP_ROUTING} from './app.routes';

// Componentes
import { CabeceraComponent } from './components/shared/cabecera/cabecera.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { MarketplaceComponent } from './components/marketplace/marketplace.component';
import { CarritoComponent } from './components/carrito/carrito.component';
import { ProductoComponent } from './components/producto/producto.component';
import { ProductoIndComponent } from './components/producto-ind/producto-ind.component';
import { BusquedaComponent } from './components/busqueda/busqueda.component';
import { LoginComponent } from './components/acceso/login/login.component';
import { RegistroComponent } from './components/acceso/registro/registro.component';


@NgModule({
  declarations: [
    AppComponent,
    CabeceraComponent,
    InicioComponent,
    FooterComponent,
    MarketplaceComponent,
    CarritoComponent,
    ProductoComponent,
    ProductoIndComponent,
    BusquedaComponent,
    LoginComponent,
    RegistroComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule,
    APP_ROUTING
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }

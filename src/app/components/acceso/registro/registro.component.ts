import { Component, OnInit } from '@angular/core';
import { UsuarioModel } from 'src/app/models/usuario.model'
import { NgForm } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.scss']
})
export class RegistroComponent implements OnInit {
  usuario:UsuarioModel;
  constructor(private auth:ApiService) { }

  ngOnInit() {
    this.usuario=new UsuarioModel();
  }

  Registrar(form:NgForm){
    if(form.invalid){console.log(form);return}
      this.auth.nuevo_usuario(this.usuario)
      .subscribe( resp =>{
        console.log(resp)
      },(err)=>{
        console.log(err);
      })

  }

}

import { Component, OnInit } from '@angular/core';
import { UsuarioModel } from 'src/app/models/usuario.model'
import { NgForm } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  usuario:UsuarioModel= new UsuarioModel();

  constructor(private auth:ApiService, private router:Router) { }

  ngOnInit() {
    this.usuario=new UsuarioModel();

  }
  Acceder(form:NgForm){
    console.log(form);
    if(form.invalid){ return}else{
      this.auth.login(this.usuario).subscribe(resp=>{
        console.log(resp)
        this.router.navigateByUrl('/inicio')
      },(err)=>{console.log(err)})
    }
    console.log(this.usuario)
  }

}

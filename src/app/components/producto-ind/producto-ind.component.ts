import { Component } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ApiService} from '../../services/api.service';


@Component({
  selector: 'app-producto-ind',
  templateUrl: './producto-ind.component.html',
  styleUrls: ['./producto-ind.component.scss']
})
export class ProductoIndComponent  {

   product_ind:any=[];

  constructor(private router: ActivatedRoute,
              private api:ApiService
            ) {
              this.router.params.subscribe(params =>{

                this.api.getProducto(params['id'])
                .subscribe((data:any) => {
                  this.product_ind=data.success;
                  console.log(this.product_ind);
                });

})

}

}

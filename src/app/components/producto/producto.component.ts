
import { Component,Input,Output, EventEmitter } from '@angular/core';
import {ApiService} from '../../services/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.scss']
})
export class ProductoComponent {
  @Input() producto: any = {};
  @Input() indice: number;
  @Output() productoSeleccionado:EventEmitter <number>;

  title = 'kalma';

   productos:any[]=[];
   loading: boolean;



  constructor(private api:ApiService,
              private router:Router){

this.productoSeleccionado = new EventEmitter();
    this.loading=true;
    this.api.getProductos()
    .subscribe((data:any) => {
      // console.log(data.success)
      this.productos=data.success;
      this.loading=false;
    });
    this.api.getProducto(1)
    .subscribe((data:any) => {
      // console.log(data.success)
    });
  }

  verProducto(idx:number){
  // console.log(this.indice);
  this.router.navigate([ '/producto', idx ])
  // this.heroeSeleccionado.emit(this.indice);
  }

}

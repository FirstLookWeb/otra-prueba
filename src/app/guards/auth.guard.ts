import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ApiService } from '../services/api.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private auth:ApiService,private router:Router){}
  canActivate(): boolean  {
    console.log('guard');
    if(this.auth.estaAutenticado()){return true}else{
      this.router.navigateByUrl('acceso');
      return false;
    }

  }

}

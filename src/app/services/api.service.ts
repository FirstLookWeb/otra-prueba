import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import { map } from 'rxjs/operators';
import {UsuarioModel} from '../models/usuario.model';


@Injectable({
  providedIn: 'root'
})
export class ApiService {
  userToken:string;
  constructor(private http:HttpClient) {
    this.leerToken();
  }
getQuery(query:string){
  const url=`/api/${query}`;
  const headers= new HttpHeaders({
    "Content-Type": "application/x-www-form-urlencoded",
    'Authorization': 'Bearer '+this.userToken
  })
  return this.http.get(url,{headers});
}

getProductos(){return this.getQuery('products');
// .pipe(map(data=>data['albums'].items))
}
getProducto(id:number){return this.getQuery(`products/${id}`);
// .pipe(map(data=>data['artists'].items))
}
private guardarToken(token:string){
  this.userToken=token;
  localStorage.setItem('token',token)
  let hoy= new Date;
  hoy.setSeconds(3600);
  localStorage.setItem('expira',hoy.getTime().toString());
}

leerToken(){
  if(localStorage.getItem('token')){
    this.userToken=localStorage.getItem('token');
  }else{
    this.userToken = '';
  }
  console.log(this.userToken);
  return this.userToken;
}

login(usuario:UsuarioModel){
  const  authData={
    nif:usuario.nif,
    password:usuario.password,
    ecosystem_id: 1  }
console.log(authData);
  const headers = new HttpHeaders()
      .set('cache-control', 'no-cache')
      .set('content-type', 'application/json')

  return this.http
             .post(`/api/login`, authData, { headers: headers })
             .pipe(
                 map(resp=>{
                   this.guardarToken(resp['success'].token);
                   return resp;
                 })
               );  }
logout(){}
nuevo_usuario(usuario:UsuarioModel){
  const  authData={
    ...usuario
    // email:usuario.email,
    // nif:usuario.nif,
    // password:usuario.password,
    // nombre:usuario.nombre,
    // ecosystem_id:1
  }
  const headers = new HttpHeaders()
      .set('cache-control', 'no-cache')
      .set('content-type', 'application/json')
      .set('Authorization','Basic a2FsbWFkbWluOmg0eVwuaG4+UVk1OGhoSV8=')
  return this.http
             .post(`/api/register`, authData, { headers: headers })
             // .subscribe(res =>{ console.log(res);  this.guardarToken(res['success'].token);});
}

   // sendPostRequest() {
   //      const headers = new HttpHeaders()
   //          .set('cache-control', 'no-cache')
   //          .set('content-type', 'application/json')
   //
   //      const body = {
   //          nif: 'Firstlook',
   //          password: 'Firstlook',
   //          ecosystem_id: 1
   //      }
   //
   //      return this.http
   //                 .post(`/api/login`, body, { headers: headers })
   //                 .subscribe(res =>{ console.log(res);  this.guardarToken(res['success'].token);});
   // }

estaAutenticado():boolean{
  if(this.userToken.length <2 ){return false;}else{return true;}
  // const expira=Number(localStorage.getItem('expira'));
  // const expiraDate = new Date();
  // expiraDate.setTime(expira);
  // if(expiraDate > new Date()){
  //   return true
  // }else{
  //   return false
  // }

}
}

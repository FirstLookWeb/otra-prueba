import {RouterModule,Routes} from '@angular/router';
import {CarritoComponent} from './components/carrito/carrito.component';
import {InicioComponent} from './components/inicio/inicio.component';
import {MarketplaceComponent} from './components/marketplace/marketplace.component';
import {ProductoIndComponent} from './components/producto-ind/producto-ind.component';
import {BusquedaComponent} from './components/busqueda/busqueda.component';
import { LoginComponent } from './components/acceso/login/login.component';
import { RegistroComponent } from './components/acceso/registro/registro.component';
import { AuthGuard } from './guards/auth.guard';


const APP_ROUTES: Routes = [
  { path: 'inicio', component: InicioComponent,canActivate:[AuthGuard] },
  { path: 'carrito', component: CarritoComponent },
  { path: 'acceso', component: LoginComponent },
  { path: 'registro', component: RegistroComponent },
  { path: 'marketplace', component: MarketplaceComponent },
  { path: 'producto/:id', component: ProductoIndComponent },
  { path: 'busqueda/:busqueda', component: BusquedaComponent },
  { path: '**', pathMatch:'full', redirectTo: 'inicio' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES,{useHash:true});
